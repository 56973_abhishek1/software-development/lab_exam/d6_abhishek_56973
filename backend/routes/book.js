const { request, response } = require("express")
const express=require("express")
const db=require("../db")
const utils=require("../utils")
const router=express.Router()

router.get("/", (request,response)=>{
    const connection= db.openConnection()
    const statement =`select * from book`
    connection.query(statement, (error, result)=>{
        if(error) response.send(utils.createResult(error))
        else{
            response.send(utils.createResult(null, result))
        }
        connection.end()
    })
})

router.post("/add", (request,response)=>{
    const{ book_title, publisher_name, author_name}=request.body;
    const connection= db.openConnection()
    const statement =`insert into book (book_title, publisher_name, author_name) values('${book_title}', '${publisher_name}','${author_name}')`;
    connection.query(statement, (error, result)=>{
        if(error) response.send(utils.createResult(error))
        else{
            response.send(utils.createResult(null, result))
        }
        connection.end()
    })
})


router.put("/update/:id", (request,response)=>{
    const{ id}=request.params;
    const { publisher_name, author_name}=request.body
    const connection= db.openConnection()
    const statement =`update book set
    publisher_name='${publisher_name}',
    author_name='${author_name}' 
    where book_id='${id}'`

    connection.query(statement, (error, result)=>{
        if(error) response.send(utils.createResult(error))
        else{
            response.send(utils.createResult(null, result))
        }
        connection.end()
    })
})

router.delete("/:id", (request,response)=>{
    const{id}=request.params
    const connection= db.openConnection()
    const statement =`delete from book where book_id='${id}'`
    connection.query(statement, (error, result)=>{
        if(error) response.send(utils.createResult(error))
        else{
            response.send(utils.createResult(null, result))
        }
        connection.end()
    })
})

module.exports=router